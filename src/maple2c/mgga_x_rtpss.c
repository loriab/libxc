/* 
  This file was generated automatically with ./maple2c.pl.
  Do not edit this file directly as it can be overwritten!!

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

  Maple version     : Maple 2017 (X86 64 LINUX)
  Maple source      : ../maple/mgga_x_rtpss.mpl
  Type of functional: work_mgga_x
*/

static void 
xc_mgga_x_rtpss_enhance(const xc_func_type *pt, xc_mgga_work_x_t *r)
{
  double t1, t2, t3, t4, t5, t6, t8, t9;
  double t10, t11, t15, t16, t17, t18, t19, t20;
  double t21, t25, t29, t30, t31, t32, t35, t36;
  double t37, t42, t43, t46, t48, t49, t52, t53;
  double t56, t57, t61, t62, t65, t66, t67, t68;
  double t72, t73, t76, t77, t78, t81, t84, t85;
  double t89, t90, t91, t93, t94, t98, t101, t104;
  double t109, t110, t114, t117, t122, t127, t128, t133;
  double t139, t146, t150, t152, t153, t156, t159, t160;
  double t161, t164, t165, t168, t172, t175, t179, t181;
  double t183, t186, t191, t196, t207, t208, t209, t221;
  double t224, t228, t231, t232, t236, t237, t245, t250;
  double t255, t256, t274, t281, t282, t295, t306, t315;
  double t320, t326, t331, t345, t380;

  mgga_x_rtpss_params *params;
 
  assert(pt->params != NULL);
  params = (mgga_x_rtpss_params * ) (pt->params);

  t1 = r->x * r->x;
  t2 = t1 * t1;
  t3 = params->c * t2;
  t4 = r->t * r->t;
  t5 = 0.1e1 / t4;
  t6 = t2 * t5;
  t8 = 0.1e1 + t6 / 0.64e2;
  t9 = t8 * t8;
  t10 = 0.1e1 / t9;
  t11 = t5 * t10;
  t15 = M_CBRT6;
  t16 = (0.10e2 / 0.81e2 + t3 * t11 / 0.64e2) * t15;
  t17 = 0.31415926535897932385e1 * 0.31415926535897932385e1;
  t18 = POW_1_3(t17);
  t19 = t18 * t18;
  t20 = 0.1e1 / t19;
  t21 = t20 * t1;
  t25 = r->t - t1 / 0.8e1;
  t29 = 0.5e1 / 0.9e1 * t25 * t15 * t20 - 0.1e1;
  t30 = params->b * t25;
  t31 = t15 * t20;
  t32 = t31 * t29;
  t35 = 0.5e1 * t30 * t32 + 0.9e1;
  t36 = sqrt(t35);
  t37 = 0.1e1 / t36;
  t42 = 0.27e2 / 0.20e2 * t29 * t37 + t31 * t1 / 0.36e2;
  t43 = t42 * t42;
  t46 = t15 * t15;
  t48 = 0.1e1 / t18 / t17;
  t49 = t46 * t48;
  t52 = 0.50e2 * t49 * t2 + 0.162e3 * t6;
  t53 = sqrt(t52);
  t56 = 0.1e1 / params->kappa;
  t57 = t56 * t46;
  t61 = sqrt(params->e);
  t62 = t61 * t2;
  t65 = params->e * params->mu;
  t66 = t17 * t17;
  t67 = 0.1e1 / t66;
  t68 = t2 * t1;
  t72 = t16 * t21 / 0.24e2 + 0.146e3 / 0.2025e4 * t43 - 0.73e2 / 0.97200e5 * t42 * t53 + 0.25e2 / 0.944784e6 * t57 * t48 * t2 + t62 * t5 / 0.720e3 + t65 * t67 * t68 / 0.2304e4;
  t73 = t61 * t15;
  t76 = 0.1e1 + t73 * t21 / 0.24e2;
  t77 = t76 * t76;
  t78 = 0.1e1 / t77;
  t81 = exp(-t72 * t78 * t56);
  r->f = 0.1e1 + params->kappa * (0.1e1 - t81);

  if(r->order < 1) return;

  r->dfdrs = 0.0e0;
  t84 = t1 * r->x;
  t85 = params->c * t84;
  t89 = params->c * t2 * t84;
  t90 = t4 * t4;
  t91 = 0.1e1 / t90;
  t93 = 0.1e1 / t9 / t8;
  t94 = t91 * t93;
  t98 = (t85 * t11 / 0.16e2 - t89 * t94 / 0.512e3) * t15;
  t101 = t20 * r->x;
  t104 = r->x * t15;
  t109 = 0.1e1 / t36 / t35;
  t110 = t29 * t109;
  t114 = t49 * r->x;
  t117 = -0.5e1 / 0.4e1 * params->b * r->x * t32 - 0.25e2 / 0.36e2 * t30 * t114;
  t122 = -0.3e1 / 0.16e2 * t104 * t20 * t37 - 0.27e2 / 0.40e2 * t110 * t117 + t104 * t20 / 0.18e2;
  t127 = 0.1e1 / t53;
  t128 = t42 * t127;
  t133 = 0.200e3 * t49 * t84 + 0.648e3 * t84 * t5;
  t139 = t61 * t84;
  t146 = t98 * t21 / 0.24e2 + t16 * t101 / 0.12e2 + 0.292e3 / 0.2025e4 * t42 * t122 - 0.73e2 / 0.97200e5 * t122 * t53 - 0.73e2 / 0.194400e6 * t128 * t133 + 0.25e2 / 0.236196e6 * t57 * t48 * t84 + t139 * t5 / 0.180e3 + t65 * t67 * t2 * r->x / 0.384e3;
  t150 = 0.1e1 / t77 / t76;
  t152 = t72 * t150 * t56;
  t153 = t73 * t101;
  t156 = -t146 * t78 * t56 + t152 * t153 / 0.6e1;
  r->dfdx = -params->kappa * t156 * t81;
  t159 = t4 * r->t;
  t160 = 0.1e1 / t159;
  t161 = t160 * t10;
  t164 = t2 * t2;
  t165 = params->c * t164;
  t168 = 0.1e1 / t90 / r->t * t93;
  t172 = (-t3 * t161 / 0.32e2 + t165 * t168 / 0.1024e4) * t15;
  t175 = t31 * t37;
  t179 = params->b * t15 * t20 * t29;
  t181 = t30 * t49;
  t183 = 0.5e1 * t179 + 0.25e2 / 0.9e1 * t181;
  t186 = 0.3e1 / 0.4e1 * t175 - 0.27e2 / 0.40e2 * t110 * t183;
  t191 = t2 * t160;
  t196 = t172 * t21 / 0.24e2 + 0.292e3 / 0.2025e4 * t42 * t186 - 0.73e2 / 0.97200e5 * t186 * t53 + 0.73e2 / 0.600e3 * t128 * t191 - t62 * t160 / 0.360e3;
  r->dfdt = t196 * t78 * t81;
  r->dfdu = 0.0e0;

  if(r->order < 2) return;

  r->d2fdrs2 = 0.0e0;
  t207 = 0.1e1 / t90 / t4;
  t208 = t9 * t9;
  t209 = 0.1e1 / t208;
  t221 = t122 * t122;
  t224 = t20 * t109;
  t228 = t35 * t35;
  t231 = t29 / t36 / t228;
  t232 = t117 * t117;
  t236 = params->b * t46;
  t237 = t48 * t1;
  t245 = -0.3e1 / 0.16e2 * t175 + 0.3e1 / 0.16e2 * t104 * t224 * t117 + 0.81e2 / 0.80e2 * t231 * t232 - 0.27e2 / 0.40e2 * t110 * (-0.5e1 / 0.4e1 * t179 + 0.25e2 / 0.72e2 * t236 * t237 - 0.25e2 / 0.36e2 * t181) + t31 / 0.18e2;
  t250 = t122 * t127;
  t255 = t42 / t53 / t52;
  t256 = t133 * t133;
  t274 = (0.3e1 / 0.16e2 * params->c * t1 * t11 - 0.11e2 / 0.512e3 * params->c * t68 * t94 + 0.3e1 / 0.8192e4 * params->c * t164 * t1 * t207 * t209) * t15 * t21 / 0.24e2 + t98 * t101 / 0.6e1 + t16 * t20 / 0.12e2 + 0.292e3 / 0.2025e4 * t221 + 0.292e3 / 0.2025e4 * t42 * t245 - 0.73e2 / 0.97200e5 * t245 * t53 - 0.73e2 / 0.97200e5 * t250 * t133 + 0.73e2 / 0.388800e6 * t255 * t256 - 0.73e2 / 0.194400e6 * t128 * (0.600e3 * t49 * t1 + 0.1944e4 * t1 * t5) + 0.25e2 / 0.78732e5 * t57 * t237 + t61 * t1 * t5 / 0.60e2 + 0.5e1 / 0.384e3 * t65 * t67 * t2;
  t281 = t77 * t77;
  t282 = 0.1e1 / t281;
  t295 = t156 * t156;
  r->d2fdx2 = -params->kappa * (-t274 * t78 * t56 + t146 * t150 * t56 * t153 / 0.3e1 - t72 * t282 * t56 * params->e * t46 * t237 / 0.24e2 + t152 * t73 * t20 / 0.6e1) * t81 - params->kappa * t295 * t81;
  t306 = t90 * t90;
  t315 = t186 * t186;
  t320 = t183 * t183;
  t326 = -0.3e1 / 0.4e1 * t31 * t109 * t183 + 0.81e2 / 0.80e2 * t231 * t320 - 0.15e2 / 0.4e1 * t110 * t236 * t48;
  t331 = t186 * t127;
  t345 = t196 * t196;
  r->d2fdt2 = ((0.3e1 / 0.32e2 * t3 * t91 * t10 - 0.7e1 / 0.1024e4 * t165 * t207 * t93 + 0.3e1 / 0.32768e5 * params->c * t164 * t2 / t306 * t209) * t15 * t21 / 0.24e2 + 0.292e3 / 0.2025e4 * t315 + 0.292e3 / 0.2025e4 * t42 * t326 - 0.73e2 / 0.97200e5 * t326 * t53 + 0.73e2 / 0.300e3 * t331 * t191 + 0.1971e4 / 0.100e3 * t255 * t164 * t207 - 0.73e2 / 0.200e3 * t128 * t2 * t91 + t62 * t91 / 0.120e3) * t78 * t81 - t345 * t282 * t56 * t81;
  r->d2fdu2 = 0.0e0;
  r->d2fdrsx = 0.0e0;
  r->d2fdrst = 0.0e0;
  r->d2fdrsu = 0.0e0;
  t380 = 0.3e1 / 0.32e2 * t104 * t224 * t183 - 0.3e1 / 0.8e1 * t31 * t109 * t117 + 0.81e2 / 0.80e2 * t231 * t117 * t183 + 0.15e2 / 0.16e2 * t110 * params->b * t114;
  r->d2fdxt = -params->kappa * (-((-t85 * t161 / 0.8e1 + 0.3e1 / 0.256e3 * t89 * t168 - 0.3e1 / 0.16384e5 * params->c * t164 * t84 / t90 / t159 * t209) * t15 * t21 / 0.24e2 + t172 * t101 / 0.12e2 + 0.292e3 / 0.2025e4 * t186 * t122 + 0.292e3 / 0.2025e4 * t42 * t380 - 0.73e2 / 0.97200e5 * t380 * t53 + 0.73e2 / 0.600e3 * t250 * t191 - 0.73e2 / 0.194400e6 * t331 * t133 - 0.73e2 / 0.1200e4 * t255 * t133 * t2 * t160 + 0.73e2 / 0.150e3 * t128 * t84 * t160 - t139 * t160 / 0.90e2) * t78 * t56 + t196 * t150 * t56 * t153 / 0.6e1) * t81 + t156 * t196 * t78 * t81;
  r->d2fdxu = 0.0e0;
  r->d2fdtu = 0.0e0;

  if(r->order < 3) return;


}

#define maple2c_order 3
#define maple2c_func  xc_mgga_x_rtpss_enhance
